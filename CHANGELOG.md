# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.2] - 2021-12-06
### Added
- correct timestamp for document and archiving indexes (second precision)

## [0.2.1] - 2021-12-03
### Added
- indexes for archiving, document and configuration

## [0.2.0] - 2021-12-2
### Changed
- Makefile for building
- CMake installation
- README
- name of the library
- version of libhdb++ (live at head), new AbstractDB interface
- bulk insert of events

### Added
- Dockefile containing the library plus event subscriber and configuration manager
- .gitlab-ci.yml
- helm chart for k8s deployment
- google testing

## [0.1.0] - 2019-09-26
### Added
- first version of library 
- version 1.0.0 of libhdb++