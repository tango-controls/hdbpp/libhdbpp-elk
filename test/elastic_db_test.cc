#include <gtest/gtest.h>
#include <vector>
#include "ElasticDB.hpp"
#include "json/json.hpp"

using namespace std;

namespace hdbpp 
{
    class ElasticDBTest : public ::testing::Test {
    protected:
        ElasticDBTest() {
            vector<string> lib_configuration = {
                "logging_enabled=true",
                "elk_http_repo=http://127.0.0.1:9200"
            };
            _ElasticDB = new ElasticDB("elasticsearch_db", lib_configuration);
        }


        ~ElasticDBTest() {
            delete (_ElasticDB);
        }

        void SetUp() override {
            _ElasticDB->add_attribute("tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw", 
                Tango::DEV_SHORT, 
                Tango::SCALAR, 
                Tango::READ_WRITE);
        }

        void TearDown() override {
            // Code here will be called immediately after each test (right
            // before the destructor).
        }

        ElasticDB* _ElasticDB;
    };

    TEST_F(ElasticDBTest, ConfigureAttribute) {
        json errors = _ElasticDB->GetErrors();
        // range-based for
        // for (auto& element : errors) {
        // std::cout << element << '\n';
        // }
        EXPECT_EQ(errors.size(), 0) << errors;
    }

    TEST_F(ElasticDBTest, InsertEvent) {
        json errors = _ElasticDB->GetErrors();

        short sh = 10;
        Tango::DeviceAttribute *attr_value_in = new Tango::DeviceAttribute("tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw", sh);

        Tango::EventData *event_data = new Tango::EventData();
        event_data->attr_name = "tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw";
        event_data->event = "event_name";
        event_data->attr_value = attr_value_in;
        event_data->err = false;

        HdbEventDataType type;
        type.attr_name = "tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw";
        type.data_type = Tango::DEV_SHORT;
        type.data_format = Tango::SCALAR;
        type.write_type = Tango::READ_WRITE;

        // try
        // {
        //     cout << "stica_cc1" << '\n';
        //     vector<short> vshort;
        //     event_data->attr_value->extract_set(vshort);
        //     cout << "stica_cc2" << '\n';
        // }
        // catch (int e)
        // {
        //     cout << "Ex#" << e << '\n';
        // }

        _ElasticDB->insert_event(event_data, type);
        EXPECT_EQ(errors.size(), 0) << errors;
    }


} // namespace

