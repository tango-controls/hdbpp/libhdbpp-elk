#include <gtest/gtest.h>
#include <vector>
#include "AttributeName.hpp"

using namespace std;

namespace hdbpp 
{
    class AttributeNameTest : public ::testing::Test {
    protected:
        void SetUp() override {
            fqdn_attr_names.push_back("tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw");
            fqdn_attr_names.push_back("acudebian7.esrf.fr:10000/test/universal/1/devshortrw");
            fqdn_attr_names.push_back("acudebian7:10000/test/universal/1/devshortrw");
        }

        string GetDomain(std::string name){
            string proto_prefix = "://";
            string port_delimiter = ":10000";
            int prefix = name.find(proto_prefix);
            
            string result;
            if (prefix >= 0) {
                result = name.substr(prefix + proto_prefix.size(), name.find(port_delimiter) - (prefix + proto_prefix.size())) + ":10000"; 
            }
            else {
                result = name.substr(0, name.find(port_delimiter)) + ":10000"; 
            }
            // std::cout << "(" << name << ", " << to_string(prefix) << ", " << name.find(port_delimiter) << ", " << result << ")" << endl;
            return result;
        }

        std::vector<string> fqdn_attr_names;
    };

    TEST_F(AttributeNameTest, Behaviour) {
        for (string fqdn_attr_name : fqdn_attr_names) {

            AttributeName attribute_name(fqdn_attr_name);

            EXPECT_EQ(
                fqdn_attr_name, 
                attribute_name.fully_qualified_attribute_name());

            EXPECT_EQ(
                "test/universal/1/devshortrw", 
                attribute_name.full_attribute_name());

            EXPECT_EQ(
                GetDomain(fqdn_attr_name), 
                attribute_name.tango_host());

            EXPECT_EQ(
                GetDomain(fqdn_attr_name), 
                attribute_name.tango_host_with_domain());

            EXPECT_EQ(
                GetDomain(fqdn_attr_name), 
                attribute_name.tango_host_with_domain());

            EXPECT_EQ(
                AttributeName::NoError, 
                attribute_name.validate_domain_family_member_name());

            EXPECT_EQ(
                "test", 
                attribute_name.domain());

            EXPECT_EQ(
                "universal", 
                attribute_name.family());

            EXPECT_EQ(
                "1", 
                attribute_name.member());

            EXPECT_EQ(
                "devshortrw", 
                attribute_name.name());
        }
    }


} // namespace