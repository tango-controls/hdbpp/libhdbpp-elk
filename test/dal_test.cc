#include <chrono>
#include <thread>
#include <gtest/gtest.h>
#include <vector>
#include "DAL.hpp"
#include "json/json.hpp"
#include "AttributeName.hpp"

using namespace std;

namespace hdbpp 
{
    class DALTest : public ::testing::Test {
    protected:
        DALTest() {
            const char* elasticsearch = std::getenv("ELASTICSEARCH");
            _DAL = new DAL(elasticsearch);
        }


        ~DALTest() {
            delete (_DAL);
        }

        void SetUp() override {
            Log::LogLevel() = Debug;
            attribute_name = new AttributeName("tango://acudebian7.esrf.fr:10000/test/universal/1/devshortrw");
            attr_conf = new AttributeConfiguration(*attribute_name);
            struct timespec ts;
            clock_gettime(CLOCK_REALTIME, &ts);
            current_time = ((int64_t)ts.tv_sec) * 1000;
            current_time_us = ts.tv_nsec / 1000;
            json value;
            value["vshort"] = 10;
            attr_event_data = new AttributeEventData(attr_conf->GetID(),
                "2021-11-29",
                current_time,
                current_time_us,
                current_time,
                current_time_us,
                current_time,
                current_time_us,
                0,
                "errors",
                value,
                value,
                attr_conf->GetTtl());
            attr_param = new AttributeParameter(attr_conf->GetID(),
                                  current_time,
                                  current_time_us,
                                  current_time,
                                  current_time_us,
                                  "label",
                                  "unit",
                                  "standard_unit",
                                  "display_unit",
                                  "format",
                                  "archive_rel_change",
                                  "archive_abs_change",
                                  "archive_period",
                                  "description");
        }

        void TearDown() override {
            delete (attribute_name);
            delete (attr_conf);
            delete (attr_event_data);
            delete (attr_param);
        }

        DAL* _DAL;
        AttributeName* attribute_name;
        AttributeConfiguration* attr_conf;
        AttributeEventData* attr_event_data;
        AttributeParameter* attr_param;
        int64_t current_time;
        int current_time_us;
    };

    TEST_F(DALTest, SaveAttributeConfiguration) {
        bool result = false;
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        int retry = 10;
        result = false;
        // the retry is needed because elasticsearch takes time for the mapping of value and type
        while (retry >=0 || !result) {
            result = _DAL->GetAttributeConfiguration(*attr_conf);
            this_thread::sleep_for(std::chrono::milliseconds(1000));
            retry--;
        }
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        
    }

    TEST_F(DALTest, SaveAttributeConfigurationHistory) {
        bool result = false;
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        AttributeConfigurationHistory attr_conf_history(*attr_conf, EVENT_ADD, current_time, current_time_us);
        //attr_conf_history.SetAttributeConfigurationID(attr_conf->GetID());
        cout << "Json for attr_conf_history -> " << attr_conf_history.ToJson();
        result = _DAL->SaveAttributeConfigurationHistory(attr_conf_history);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        cout << "JsonQuery for attr_conf_history -> " << attr_conf_history.GetJsonQuery() << '\n';
        int retry = 10;
        result = false;
        while (retry >=0 || !result) {
            result = _DAL->GetAttributeConfigurationHistory(attr_conf_history);
            // this_thread::sleep_for(std::chrono::milliseconds(100));
            retry--;
        }
        result = _DAL->GetAttributeConfigurationHistory(attr_conf_history);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
    }

    TEST_F(DALTest, SaveAttributeParameter){
        bool result = false;
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        attr_param->SetAttributeConfigurationID(attr_conf->GetID());
        cout << attr_param->ToJson() << '\n';
        result = _DAL->SaveAttributeParameter(*attr_param);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
    }

    TEST_F(DALTest, SaveAttributeEventData){
        bool result = false;
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        attr_event_data->SetAttributeConfigurationId(attr_conf->GetID());
        result = _DAL->SaveAttributeEventData(*attr_event_data);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
    }

    TEST_F(DALTest, SaveDocument){
        bool result = false;
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        attr_event_data->SetAttributeConfigurationId(attr_conf->GetID());
        time_t rawtime;
        struct tm * timeinfo;
        time (&rawtime);
        timeinfo = localtime (&rawtime);
        // Document doc(*attr_conf, *attr_event_data, asctime(timeinfo));

        char buffer[80];
        strftime(buffer,sizeof(buffer),"%Y-%m-%dT%H:%M:%SZ",timeinfo);
        string str(buffer);

        Document doc(*attr_conf, *attr_event_data, str);

        result = _DAL->SaveDocument(doc);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
    }

    TEST_F(DALTest, SaveDocumentFile){
        bool result = false;
        string output_filename="/tmp/filename.archiver";
        _DAL->SetWrite2File(output_filename);
        if (!_DAL->GetAttributeConfiguration(*attr_conf)) {
            result = _DAL->SaveAttributeConfiguration(*attr_conf);
            EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        }
        attr_event_data->SetAttributeConfigurationId(attr_conf->GetID());
        time_t rawtime;
        struct tm * timeinfo;
        time (&rawtime);
        timeinfo = localtime (&rawtime);
        // Document doc(*attr_conf, *attr_event_data, asctime(timeinfo));

        char buffer[80];
        strftime(buffer,sizeof(buffer),"%Y-%m-%dT%H:%M:%SZ",timeinfo);
        string str(buffer);

        Document doc(*attr_conf, *attr_event_data, str);

        result = _DAL->SaveDocument(doc);
        EXPECT_EQ(result, true) << "Errors -> " << _DAL->GetErrors() << " QueryResult -> " << _DAL->GetQueryResultArray();
        string data;
        ifstream infile; 
        strftime(buffer,sizeof(buffer),"%Y-%m-%d",timeinfo);
        string postfix(buffer);
        infile.open(output_filename+"_"+postfix);         
        cout << "Reading from the file" << endl; 
        infile >> data;
        int len = data.length();
        EXPECT_GE(len, 0) << "Errors in writing file to disk";
        infile.close();
        _DAL->SetWrite2File("");
    }

} // namespace

