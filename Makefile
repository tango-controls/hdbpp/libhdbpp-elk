.PHONY: clean build test all image-build run-test-on-container run-test-on-container-no-build start-elastic-container

CMAKE      := "cmake"
BUILD_DIR  := build
SOURCE_DIR := ..
CMAKE_ARGS := -G "Unix Makefiles" "-DCMAKE_INCLUDE_PATH=/usr/local/include/tango;/usr/local/include/hdb++" "-DCMAKE_CXX_FLAGS=-ggdb3" ..
RELEASE    := test
CHART      := charts/elastic-archiver
NAMESPACE  := default

VERSION ?= 0.2.2
ELASTICSEARCH ?= http://127.0.0.1:9200

build: # build the project
	@mkdir -p $(BUILD_DIR) && cd $(BUILD_DIR) && $(CMAKE) $(CMAKE_ARGS) && make

test: # test the project with google test
	@mkdir -p $(BUILD_DIR) && cd $(BUILD_DIR) && ELASTICSEARCH=$(ELASTICSEARCH) ctest --output-on-failure

clean: # delete files from build and lib directory
	@rm -rf $(BUILD_DIR)/*; rm -rf lib/*

all: clean build test # clean build and test the project with google test

image-build: # build the project
	@docker build . -t elastic-db-lib:$(VERSION) && \
	 docker tag elastic-db-lib:$(VERSION) registry.gitlab.com/tango-controls/hdbpp/libhdbpp-elk/elastic-db-lib:$(VERSION)

image-push: # push the image into gitlab registry
	@echo $(CI_REGISTRY_PASSWORD) | docker login -u $(CI_REGISTRY_USER) --password-stdin $(CI_REGISTRY) && \
	docker push elastic-db-lib:$(VERSION)

run-test-on-container: image-build # build and run tests on container
	@docker run --network host elastic-db-lib:$(VERSION) sh -c 'make test'

run-test-on-container-no-build: # run tests on container
	@docker run --network host elastic-db-lib:$(VERSION) sh -c 'make test'

start-elastic-container: ## start an elastisearch server in a container with port forwarding 9200 and 9300
	@docker run -d -p 127.0.0.1:9200:9200 -p 127.0.0.1:9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:7.15.2

install-chart: ## install chart for elastic archiving
	@helm repo add elastic https://helm.elastic.co && \
	helm dependency update $(CHART) && \
	helm upgrade $(RELEASE) --install $(CHART) -n $(NAMESPACE) && \
	kubectl patch statefulsets.apps hdbpp-es-es01 --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/volumeMounts/1/readOnly", "value": false}]' && \
	kubectl patch statefulsets.apps hdbpp-cm-cm01 --type='json' -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/volumeMounts/1/readOnly", "value": false}]'

template-chart: ## install chart for elastic archiving
	@helm repo add elastic https://helm.elastic.co && \
	helm dependency update $(CHART) && \
	helm template $(RELEASE) $(CHART) -n $(NAMESPACE) | tee output.yaml

uninstall-chart: ## uninstall chart for elastic archiving
	@helm uninstall $(RELEASE) -n $(NAMESPACE)

reinstall-chart: uninstall-chart install-chart ## re-install chart for elastic archiving