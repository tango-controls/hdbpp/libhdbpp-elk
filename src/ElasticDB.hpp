
#ifndef _HDBPP_EXAMPLE_IMPL_HPP
#define _HDBPP_EXAMPLE_IMPL_HPP

#include "Log.hpp"
#include "AttributeName.hpp"
#include "LibHdb++Defines.hpp"

#include <hdb++/AbstractDB.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <stdint.h>
#include <tango.h>

#include "json/json.hpp"
using json = nlohmann::json;
#include "restclient-cpp/connection.h"
#include "restclient-cpp/restclient.h"
#include "DataObjects.hpp"
#include "DAL.hpp"

namespace hdbpp
{
class ElasticDB : public AbstractDB
{
public:
    // Takes a list of configuration parameters to start the driver with
    ElasticDB(const string &id, const std::vector<std::string> &configuration);

    virtual ~ElasticDB();

    // Inserts an attribute archive event for the EventData into the database. If the attribute
    // does not exist in the database, then an exception will be raised. If the attr_value
    // field of the data parameter if empty, then the attribute is in an error state
    // and the error message will be archived.
    void insert_event(Tango::EventData *event_data, const HdbEventDataType &data_type) override;

    // Insert multiple attribute archive events. Any attributes that do not exist will
    // cause an exception. On failure the fall back is to insert events individually
    void insert_events(std::vector<std::tuple<Tango::EventData *, HdbEventDataType>> events) override;

    // Inserts the attribute configuration data (Tango Attribute Configuration event data)
    // into the database. The attribute must be configured to be stored in HDB++,
    // otherwise an exception will be thrown.
    void insert_param_event(Tango::AttrConfEventData *param_event, const HdbEventDataType & /* data_type */) override;

    // Add an attribute to the database. Trying to add an attribute that already exists will
    // cause an exception
    void add_attribute(const std::string &fqdn_attr_name, int type, int format, int write_type) override;

    // Update the attribute ttl. The attribute must have been configured to be stored in
    // HDB++, otherwise an exception is raised
    void update_ttl(const std::string &fqdn_attr_name, unsigned int ttl) override;

    // Inserts a history event for the attribute name passed to the function. The attribute
    // must have been configured to be stored in HDB++, otherwise an exception is raised.
    // This function will also insert an additional CRASH history event before the START
    // history event if the given event parameter is DB_START and if the last history event
    // stored was also a START event.
    void insert_history_event(const std::string &fqdn_attr_name, unsigned char event) override;

    // Check what hdbpp features this library supports.
    bool supported(HdbppFeatures feature) override;

    const json& GetErrors() const;

private:
    // cache the attribute name to some of its often used data, i.e. ttl and id. This
    // saves it being looked up in the database everytime we request it
    map<string, AttributeParams> attribute_cache;

    // ELK web site (no end slash!) i.e. 'http://197.234.23.1:9200' or '197.234.23.1:9200'
    string elk_http_repo;
    DAL* _DAL;
    string _id;

    string get_data_type(int type /*DEV_DOUBLE, DEV_STRING, ..*/,
                         int format /*SCALAR, SPECTRUM, ..*/,
                         int write_type /*READ, READ_WRITE, ..*/) const;

    void string_vector2map(vector<string> str, string separator, map<string, string>* results);

    json extract_read(Tango::EventData* data, HdbEventDataType ev_data_type);

    json extract_set(Tango::EventData* data, HdbEventDataType ev_data_type);
};

class ElasticDbFactory : public DBFactory
{
public:
    // return a new HdbppExampleDb object
    virtual AbstractDB *create_db(const string &id, const std::vector<std::string> &configuration);
};


} // namespace hdbpp
#endif // _HDBPP_EXAMPLE_IMPL_HPP
