FROM artefact.skao.int/ska-tango-images-tango-cpp:9.3.9

USER root

COPY . /app

RUN apt update && apt install -y git libssl-dev build-essential pkg-config curl zip libcurl4-openssl-dev libtool m4 automake wget

WORKDIR /

RUN wget https://cmake.org/files/v3.22/cmake-3.22.0-linux-x86_64.tar.gz && \
    tar xf cmake-3.22.0-linux-x86_64.tar.gz

ENV PATH="/cmake-3.22.0-linux-x86_64/bin:${PATH}" 

WORKDIR /

RUN git clone https://gitlab.com/tango-controls/hdbpp/libhdbpp.git && \
    cd libhdbpp && \
    # git fetch --all --tags --prune && \
    # git checkout tags/v2.0.0 && \
    mkdir build && \
    cd build && \
    cmake ../ && \
    make && \
    make install

WORKDIR /

RUN git clone https://github.com/mrtazz/restclient-cpp.git && \
    cd restclient-cpp/ && \
    git fetch --all --tags --prune && \
    git checkout tags/0.5.2 && \
    ./autogen.sh && \
    ./configure && \
    make install

WORKDIR /app

RUN rm -rf build/*; make && cd build && make install

WORKDIR /

RUN git clone https://gitlab.com/tango-controls/hdbpp/hdbpp-es.git && \
    cd hdbpp-es && \
    mkdir build && \
    cd build && \
    cmake "-DCMAKE_INCLUDE_PATH=/usr/local/include/tango;/usr/local/include/hdb++" ../ && \
    make && \
    make install

WORKDIR /

RUN git clone https://gitlab.com/tango-controls/hdbpp/hdbpp-cm.git && \
    cd hdbpp-cm && \ 
    mkdir build && \
    cd build && \
    cmake "-DCMAKE_INCLUDE_PATH=/usr/local/include/tango;/usr/local/include/hdb++" ../ && \
    make && \
    make install

RUN mv /usr/local/bin/hdb++cm-srv /usr/local/bin/hdbppcm
RUN mv /usr/local/bin/hdb++es-srv /usr/local/bin/hdbppes
