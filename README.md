# Libhdbpp-ElasticSearch

[HDB++](http://www.tango-controls.org/community/projects/hdbplus) library for Apache ElasticSearch backend. This library is loaded by [Libhdbpp](https://gitlab.com/tango-controls/hdbpp/libhdbpp-elk) to archive events from a Tango Controls system.

## Documentation

* See the tango documentation [here](http://tango-controls.readthedocs.io/en/latest/administration/services/hdbpp/index.html#hdb-an-archiving-historian-service) for broader information about the HB++ archiving system and its integration into Tango Controls
* Libhdbpp-ElasticSearch [CHANGELOG.md](CHANGELOG.md) contains the latest changes both released and in development.

## Bugs Reports

Please file bug reports above in the issues section.

## Building Testing and k8s installation

See the [INSTALL.md](INSTALL.md) file for  detailed instructions on how to build and install libhdbpp-ElasticSearch.

## Docker testing

To run tests without installing everything on the local machine and without running an elasticsearch cluster, it is possible to use docker. 

In order to do that, the first step is to start a detached single node elasticsearch cluster:

```bash
make start-elastic-container
```

That makefile target will start a container with a forwarded port 9200 to the local machine. Check that is it working with:

```bash
curl localhost:9200
```

Create the necessary indexes with the following commands:

```bash
curl -X PUT "localhost:9200/document?pretty"
curl -X PUT "localhost:9200/archiving?pretty"
curl -X PUT "localhost:9200/archiverconfig?pretty"
```

Build the container image of the library and run google tests in it  with the following command: 

```bash
make run-test-on-container 
```

## Kubernetes installation

This project is structured to use k8s for development and testing so that the build environment, test environment and test results are all completely reproducible and are independent of host environment. It uses ``make`` to provide a consistent UI (run ``make help`` for targets documentation).

### Install docker

Follow the instructions available at [here](https://docs.docker.com/get-docker/).

### Install minikube

You will need to install `minikube` or equivalent k8s installation in order to set up your test environment. You can follow the instruction at [here](https://gitlab.com/ska-telescope/sdi/deploy-minikube/):
```bash
git clone git@gitlab.com:ska-telescope/sdi/deploy-minikube.git
cd deploy-minikube
make all
eval $(minikube docker-env)
```

*Please note that the command `eval $(minikube docker-env)` will point your local docker client at the docker-in-docker for minikube. Use this only for building the docker image and another shell for other work.*

### Install addon for elasticsearch kubernetes installation

In order to properly support the required persistent volume claims for the Elasticsearch StatefulSet, the default-storageclass and storage-provisioner minikube addons must be enabled.
Refer to [elasticsearch]( https://github.com/elastic/helm-charts/tree/main/elasticsearch/examples/minikube) minikube installation for more information. 

```bash
minikube addons enable default-storageclass
minikube addons enable storage-provisioner
```

### Install helm chart

Once installed docker, minikube and the addons for elasticsearch, run the following commands to install:

```bash
eval $(minikube docker-env)
make image-build
make install-chart
```

Check the running components with the following commands:

```bash
watch kubectl get pod
```

Configure an attribute by starting an ipython console inside the minikube cluster:

```bash
kubectl exec -it ska-tango-base-itango-console -- ipython
```

and then from the python console, run: 

```bash
import tango
cm = tango.DeviceProxy('archiver/elastic/cm01')
cm.SetAttributeName = 'sys/tg_test/1/short_scalar_ro'
cm.SetStrategy = "ALWAYS"
cm.SetPollingPeriod = 1000 # 1ss
cm.SetPeriodEvent = 1000 # 1ss
cm.SetArchiver = "archiver/elastic/es01"
cm.AttributeAdd()
cm.AttributeStart("sys/tg_test/1/short_scalar_ro")
```

Retrieve kibana dashboard url with the following command:

```bash
echo "http://$(minikube ip):$(kubectl get svc --selector=app=kibana --output=jsonpath={.items..spec.ports..nodePort})"
```

Check elasticsearch archiving running a curl command inside the itango pod. This means to bash into the pod with the following command:

```bash
kubectl exec -it ska-tango-base-itango-console -- bash
```

and, inside the pod, run: 

```bash
curl -X GET "elasticsearch-master-headless:9200/archiving/doc/_search?pretty"
```

## License

The source code is released under the LGPL3 license and a copy of this license is provided with the code.
